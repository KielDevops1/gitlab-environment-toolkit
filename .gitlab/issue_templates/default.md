<!-- For new feature requests, please check if the feature follows our Technical Design guidelines - https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/TECHNICAL_DESIGN.md#integrations-list. -->

<!-- For bugs, please fill in details about your GET configuration:

- GET version:
- Cloud Provider: GCP/AWS/Azure/Other
- Environment configuration:

Before raising a bug, please read through our Troubleshooting guide to determine if it's a known issue - https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_troubleshooting.md.
-->



<!-- Set Work Type label(s) for the Issue - https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification -->
<!-- Note: Only one subtype should be selected -->
/label ~type::
/label ~bug:: ~feature:: ~maintenance::

/label ~"section::core platform"
