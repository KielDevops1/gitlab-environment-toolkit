---
- name: Perform Common Tasks
  block:
    - name: Gather facts (common)
      block:
        - name: Gather facts (common)
          setup:

        - name: Mark facts gathered
          set_fact:
            ansible_facts_gathered: true
      when: not ansible_facts_gathered
      tags: always

    - name: Gather other facts (common)
      block:
        - name: Gather package facts (common)
          package_facts:
          when: ansible_facts.packages is not defined
      tags: always

    - name: Set Internal Hostname to match inventory
      hostname:
        name: "{{ inventory_hostname }}"
      when:
        - cloud_provider == 'aws'
        - ansible_hostname != inventory_hostname
      tags:
        - hostname
        - common_prep

    - name: Check if GitLab repository exists
      stat:
        path: "{{ gitlab_repo_list_path[ansible_facts['os_family']] }}"
      register: gitlab_repo_file
      tags: gpg

    - name: Configure GitLab repository GPG key
      import_tasks: gpg.yml
      when:
        - omnibus_node
        - gitlab_repo_file.stat.exists
        - not offline_setup
      tags:
        - gpg
        - common_prep

    - name: Setup Packages
      import_tasks: packages_setup.yml
      when: not offline_setup
      tags: packages

    # https://about.gitlab.com/blog/2019/11/15/tracking-down-missing-tcp-keepalives/
    - name: Configure TCP keepalive settings
      sysctl:
        name: net.ipv4.tcp_keepalive_time
        value: '300'
        sysctl_set: true
        state: present
        reload: true
      tags:
        - sysctl
        - common_prep

    - name: Mount any data disks
      block:
        - name: Get Group Data Disks Config
          set_fact:
            group_data_disks: "{{ lookup('vars', gitlab_node_type + '_data_disks', default=[]) }}"
          tags: nvme

        - name: Mount data disks
          import_tasks: data_disks.yml
          when: group_data_disks | length > 0
      when: cloud_provider != 'none'
      tags:
        - data_disks
        - common_prep

    # Create select GitLab folders if missing. Allows for setup before reconfigure.
    - name: Create select GitLab dirs if missing
      file:
        path: "{{ item.path }}"
        owner: root
        group: root
        mode: "{{ item.mode }}"
        state: directory
      loop:
        - { path: '/etc/gitlab', mode: '0775' }   # GitLab Issue: https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5814
        - { path: '/etc/gitlab/ssl', mode: '0755' }
        - { path: '/etc/gitlab/trusted-certs', mode: '0755' }
      when: omnibus_node
      tags:
        - reconfigure
        - common_prep

    # Workaround for https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/6364
    - name: Create skip-unmigrated-data-check file
      file:
        path: /etc/gitlab/skip-unmigrated-data-check
        state: touch
      when: omnibus_node
      tags:
        - reconfigure
        - common_prep

    - name: Get GitLab json config file stats if it exists
      stat:
        path: "/opt/gitlab/embedded/nodes/{{ ansible_fqdn }}.json"
      register: gitlab_json_config_file
      tags:
        - reconfigure
        - common_prep

    - name: Delete GitLab json config file if malformed
      file:
        path: "/opt/gitlab/embedded/nodes/{{ ansible_fqdn }}.json"
        state: absent
      when:
        - gitlab_json_config_file.stat.exists
        - gitlab_json_config_file.stat.size < 500
      tags:
        - reconfigure
        - common_prep

    - name: Downgrade prerequisites check
      fail:
        msg: |
          WARNING - GitLab package setup has been configured to allow downgrades but acknowledgment is missing.

          It's strongly recommended to read the documentation in full before proceeding - https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_upgrades.md#environment-downgrades

          Downgrades require prerequisites to be in place beforehand and have manual steps to be performed after.
          Attempting to downgrade without ensuring all prerequisites are met has a significant risk of breaking your environment and can result in downtime or data loss.

          Once prerequisitves are in place and additional manual steps are understood in full, rerun with `gitlab_downgrade_prerequisites_met` set to `true` at your own risk.
      when:
        - gitlab_repo_package_allow_downgrade or gitlab_deb_allow_downgrade or gitlab_rpm_allow_downgrade
        - not gitlab_downgrade_prerequisites_met
      tags: reconfigure

    - name: Unlock GitLab package installs
      command: "{{ 'yum versionlock delete' if ansible_facts['os_family'] == 'RedHat' else 'aptitude unhold' }} {{ gitlab_edition }}"
      register: result
      retries: 30
      delay: 10
      until: result is success
      failed_when:
        - result.rc != 0
        - ('no matches' not in result.stderr)
      when:
        - omnibus_node
        - gitlab_edition in ansible_facts.packages

    - name: Install GitLab package via repo if configured
      block:
        # Removes the official release repo if it's present as we typically want to stick with Nightly
        # Prevents apt from switching to the release channel (when an official release is younger than nightly)
        - name: Ensure only GitLab Nightly apt repo is installed unless specified otherwise (Ubuntu / Debian)
          file:
            path: '/etc/apt/sources.list.d/gitlab_{{ gitlab_edition }}.list'
            state: absent
          when:
            - omnibus_node
            - gitlab_repo_script_url == "https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh"
            - ansible_facts['os_family'] == "Debian"

        - name: Download GitLab repository installation script
          get_url:
            url: "{{ gitlab_repo_script_url }}"
            dest: "{{ gitlab_repo_script_path }}"
            force: true
          register: repo_file_download
          retries: 20
          delay: 5
          until: repo_file_download is success

        - name: Install GitLab repository
          command: "bash {{ gitlab_repo_script_path }}"
          register: result
          retries: 2
          delay: 10
          until: result is success
          when: repo_file_download.changed or (not gitlab_repo_file.stat.exists)

        # Install GitLab Package via repo. State of latest means always run apt install
        # If gitlab_version is passed then this will always install that version
        - name: Install GitLab repo package (deb)
          apt:
            name: "{{ gitlab_repo_package }}"
            allow_downgrade: "{{ gitlab_repo_package_allow_downgrade }}"
            state: "{{ 'present' if gitlab_version != '' else 'latest' }}"
            lock_timeout: 360
            allow_change_held_packages: true
          register: result
          retries: 12
          delay: 5
          until: result is success
          when: ansible_facts['os_family'] == "Debian"

        - name: Install GitLab repo package (rpm)
          yum:
            name: "{{ gitlab_repo_package }}"
            allow_downgrade: "{{ gitlab_repo_package_allow_downgrade }}"
            state: "{{ 'present' if gitlab_version != '' else 'latest' }}"
            lock_timeout: 360
          register: result
          retries: 12
          delay: 5
          until: result is success
          when: ansible_facts['os_family'] == 'RedHat'
      when:
        - omnibus_node
        - gitlab_repo_package != ''
        - gitlab_deb_download_url == '' and gitlab_rpm_download_url == ''
        - gitlab_deb_host_path == '' and gitlab_rpm_host_path == ''
        - not offline_setup

    - name: Install GitLab deb package if given
      block:
        - name: Download GitLab deb package
          get_url:
            url: "{{ gitlab_deb_download_url }}"
            dest: "{{ gitlab_deb_target_path }}"
            force: true
            headers: "{{ gitlab_deb_download_url_headers }}"
          register: download_deb_package_result
          until: download_deb_package_result is succeeded
          retries: 3
          delay: 30
          when:
            - omnibus_node
            - gitlab_deb_download_url != ''

        - name: Copy GitLab deb package
          copy:
            src: "{{ gitlab_deb_host_path }}"
            dest: "{{ gitlab_deb_target_path }}"
            mode: "0755"
            force: true
          when:
            - omnibus_node
            - gitlab_deb_host_path != ''

        - name: Install GitLab deb package
          apt:
            deb: "{{ gitlab_deb_target_path }}"
            allow_change_held_packages: true
            allow_downgrade: "{{ gitlab_deb_allow_downgrade }}"
          when:
            - omnibus_node
            - (gitlab_deb_host_path != '') or (gitlab_deb_download_url != '')
      when: ansible_facts['os_family'] == "Debian"

    - name: Install GitLab rpm package if given
      block:
        - name: Download GitLab rpm package
          get_url:
            url: "{{ gitlab_rpm_download_url }}"
            dest: "{{ gitlab_rpm_target_path }}"
            force: true
            headers: "{{ gitlab_rpm_download_url_headers }}"
          register: download_rpm_package_result
          until: download_rpm_package_result is succeeded
          retries: 3
          delay: 30
          when:
            - omnibus_node
            - gitlab_rpm_download_url != ''

        - name: Copy GitLab rpm package
          copy:
            src: "{{ gitlab_rpm_host_path }}"
            dest: "{{ gitlab_rpm_target_path }}"
            mode: "0755"
            force: true
          when:
            - omnibus_node
            - gitlab_rpm_host_path != ''

        - name: Install GitLab rpm package
          yum:
            name: "{{ gitlab_rpm_target_path }}"
            disable_gpg_check: true
            allow_downgrade: "{{ gitlab_rpm_allow_downgrade }}"
          when:
            - omnibus_node
            - (gitlab_rpm_host_path != '') or (gitlab_rpm_download_url != '')
      when: ansible_facts['os_family'] == 'RedHat'

    - name: Lock GitLab package updates
      command: "{{ 'yum versionlock' if ansible_facts['os_family'] == 'RedHat' else 'aptitude hold' }} {{ gitlab_edition }}"
      register: result
      retries: 60
      delay: 5
      until: result is success
      when: omnibus_node
      tags: packages

    - name: Upgrade Packages
      import_tasks: packages_upgrade.yml
      when: not offline_setup
      tags: packages

    - name: Configure Custom Config
      block:
        - name: Check if custom config exists
          stat:
            path: "{{ common_custom_config_file }}"
          delegate_to: localhost
          become: false
          register: common_custom_config_file_path

        - name: Setup Custom Config
          template:
            src: "{{ common_custom_config_file }}"
            dest: "/etc/gitlab/gitlab.common.custom.rb"
            mode: '0600'
          when: common_custom_config_file_path.stat.exists

        - name: Remove old Custom Config if not configured
          file:
            path: "/etc/gitlab/gitlab.common.custom.rb"
            state: absent
          when: not common_custom_config_file_path.stat.exists
      when: omnibus_node
      tags: reconfigure

    - name: Copy over any Custom Files
      copy:
        src: "{{ item.src_path }}"
        dest: "{{ item.dest_path }}"
        mode: "{{ item.mode if item.mode is defined else 'preserve' }}"
      loop: "{{ common_custom_files_paths }}"
      tags: reconfigure

    - name: Run Custom Tasks
      block:
        - name: Check if Custom Tasks file exists
          stat:
            path: "{{ common_custom_tasks_file }}"
          register: common_custom_tasks_file_path
          delegate_to: localhost
          become: false

        - name: Run Custom Tasks
          include_tasks:
            file: "{{ common_custom_tasks_file }}"
            apply:
              tags: custom_tasks
          when: common_custom_tasks_file_path.stat.exists
      tags: custom_tasks

    - name: Mark common has run
      set_fact:
        common_performed: true
  when: common_performed is not defined
